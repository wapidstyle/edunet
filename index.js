//
// Edunet WWW
// Main server for standalone usage
//
// This code is under the MIT license.
//
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const winston = require("winston");

winston.loggers.add("main", {
  console: {
    level: "info",
    colorize: true,
    label: "main"
  }
});
const logger = winston.loggers.get("main");

const mongoPassword = process.env.CUSTOM_MONGO_PASSWORD || "test";
const mongoPort = process.env.OPENSHIFT_MONGODB_DB_PORT || 27017;
const mongoHost = process.env.OPENSHIFT_MONGODB_DB_HOST || "localhost";
const mongoDb = process.env.OPENSHIFT_MONGODB_DB_HOST !== undefined ? "/unknown" : "/edunet";
mongoose.connect("mongodb://edunet:" + mongoPassword + "@" + mongoHost + ":" + mongoPort + mongoDb);

logger.info("Connecting to database....");

mongoose.connection.on("error", (err) => {
  logger.error("Failed to connect to MongoDB, is it up?");
  logger.error(err.message);
  process.exit(-1);
});

mongoose.connection.on("open", () => {
  logger.info("Connected to DB, running pre-server tasks.....");
  mongoose.Promise = require("q").Promise;
  logger.info("Running server...");
  require("./src/controllers")(app);
  app.listen(8080);
  logger.info("Listening on :8080");
});

// *Because there's nowhere else to put it.
