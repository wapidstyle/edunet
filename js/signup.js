function genPreview() {
  var failed = false;
  for (var c = 0; c < 3; c += 1) {
    switch (c) {
      case 0:
        if (document.getElementById("name").value === "") {
          document.getElementById("name").style.backgroundColor = "#c21";
          failed = true;
        }
        break;
      case 1:
        if (document.getElementById("password").value === "") {
          document.getElementById("password").style.backgroundColor = "#c21";
          failed = true;
        }
        break;
      case 2:
        if (document.getElementById("email").value === "") {
          document.getElementById("email").style.backgroundColor = "#c21";
          failed = true;
        }
    }
  }
  if (failed) {
    return null;
  }
  output = "";
  output += document.getElementById("bits").value;
  output += document.getElementById("type").value.substring(0, 1);
  output += " ";
  for (var i = 0; i < (Number(document.getElementById("bits").value)) / 96; i += 1) {
    output += window.btoa(i + Math.random() * 365543 + Math.random()).substring(0, 1);
  }
  output += " ";
  output += document.getElementById("name").value;
  if (document.getElementById("comment").value !== "") {
    output += "(";
    output += document.getElementById("comment").value;
    output += ")";
  }
  output += " &lt;";
  output += document.getElementById("email").value;
  output += "&gt;";
  document.getElementById("preview").innerHTML = output;
  return true;
}

function checkpassword() {
  // THIS FUNCTION IS AS SECURE AS IT CAN REASONABLY BE.
  //
  // It uses zxcvbn by Dropbox to measure the passwords.
  // IF YOU DO NOT WANT THIS FUNCTION: Modify the onkeypress of the <input> to remove this function.

  switch (zxcvbn(document.getElementById("password").value).score) {
    case 0:
      document.getElementById("password_strength").innerHTML = "is this even a password?";
      break;
    case 1:
      document.getElementById("password_strength").innerHTML = "meh";
      break;
    case 2:
      document.getElementById("password_strength").innerHTML = "ok, I guess";
      break;
    case 3:
      document.getElementById("password_strength").innerHTML = "pretty good";
      break;
    case 4:
      document.getElementById("password_strength").innerHTML = "<span style=\"color:#0F0;\">awesome!</span>";
      break;
  }
}

function checkemail() {
  if (document.getElementById("email").value === "dummy@dumm") {
    document.getElementById("mit").disabled = true;
    document.getElementById("warning").style.display = "block";
  } else {
    document.getElementById("mit").disabled = false;
    document.getElementById("warning").style.display = "none";
  }
}

function go() {
  var failed = false;
  // Check all necessary fields are filled
  for (var c = 0; c < 3; c += 1) {
    switch (c) {
      case 0:
        if (document.getElementById("name").value === "") {
          document.getElementById("name").style.backgroundColor = "#c21";
          failed = true;
        }
        break;
      case 1:
        if (document.getElementById("password").value === "") {
          document.getElementById("password").style.backgroundColor = "#c21";
          failed = true;
        }
        break;
      case 2:
        if (document.getElementById("email").value === "") {
          document.getElementById("email").style.backgroundColor = "#c21";
          failed = true;
        }
    }
  }
  if (failed) {
    return null;
  }

  // Ok, let's go!
  var options = {
    userIds: [
      {
        name: document.getElementById("name").value,
        email: document.getElementById("email").value
      }
    ],
    numBits: Number(document.getElementById("bits").value),
    passphrase: document.getElementById("password").value
  };

  openpgp.generateKey(options).then(function(key) {
    var output = "";
    output += "<p><strong>You will need to keep some data.</strong> This data";
    output += " is <em>VERY IMPORTANT</em>, and your fancy new PGP key is not";
    output += " nearly as useful, AND if someone gets your private key then";
    output += " they can a) impersonate you and b) you can't stop them. You";
    output += " can also encrypt messages, which is cool.</p>";
    output += "<br /><br />Public Key (keep in pubkey.asc):<br /><pre style=\"";
    output += "background-color:#CCC\">";
    output += key.publicKeyArmored;
    output += "</pre><br />Private Key (<strong>KEEP SECRET!</strong>)(store i";
    output += "n privkey.asc):<pre st";
    output += "yle=\"background-color:#CCC\">";
    output += key.privateKeyArmored;
    output += "</pre><hr /><p>To import your key into your computer for en/dec";
    output += "rypting messages, use the following command:<br /><pre>gpg --im";
    output += "port file.asc</pre>";

    document.getElementById("content").innerHTML = output;
  });
}
