//
// Edunet GetFile API
// A stream-based file-getting IP
//
// Under the MIT license.
//
//jshint -W097
"use strict";
const fs = require("graceful-fs");
const handpipe = require("handpipe");
const cache = require("flat-cache").load("pages");
const StreamCache = require("stream-cache");
const passthrough = () => {return require("through2")()};

require("winston").loggers.add("getfile", {
  console: {
    level: "info",
    colorize: true,
    label: "get-file"
  }
});

require("winston").loggers.add("cache", {
  console: {
    level: "info",
    colorize: true,
    label: "cache"
  }
});

const logger = require("winston").loggers.get("getfile");

function getFile(res, file, handpipeData) {
  fs.access(__dirname + "/" + file, (err) => {
    try {
      if (err) {
        throw err;
      } else {
        // TODO(wapidstyle) Make this a switch gate for more compat
        if (file.endsWith(".js")) {
          res.header("Content-Type", "text/javascript");
        }
        let handpipeStream = handpipeData ? handpipe : passthrough;
        fs.createReadStream(__dirname + "/" + file)
          .pipe(handpipeStream(handpipeData || {}))
          .pipe(res);
      }
    } catch (e) {
      logger.warn("Something bad happened: " + e.message);
      if (e.code === "ENOENT") {
        res.status(404).send("<!DOCTYPE html><html><head><title>Error 404</title></head>" +
          "<body><h1>Error 404</h1><p>The requested resource was not found</p>" +
          "<hr /><em>Edunet version 1.0.0</em></body></html>");
      } else {
        res.status(500).send("<!DOCTYPE html><html><head><title>Error 500</title></head>" +
          "<body><h1>Error 500</h1><p>An unknown error occurred.</p>" +
          "<hr /><em>Edunet version 1.0.0</em></body></html>");
      }
    }
  });
}

module.exports = getFile;
