//
// Login Manager for Edunet 1.0.0
//
// This code is under the MIT license.
//
const tokens = require("./database").mainDb.getCollection("tokens");
const sha256 = require("js-sha256").sha256;
const clientIp = require("client-ip");

function loginManager(req, res, next) {
  if(req.url.startsWith("/edunet")) {
    if (!(req.url === "/edunet/signin" || req.url === "/edunet/signup") &&
        !(req.url.indexOf(".js") !== -1 || req.url.indexOf(".css") !== -1)) {
      if (req.cookies.token === undefined || tokens.findOne({token: req.cookies.token}) === null) {
        res.redirect("/edunet/signin");
        res.end();
      } else {
        const completeToken = req.cookies.token;
        const token = completeToken.substring(0, 64);
        const date = completeToken.substring(64);
        const generatedToken = sha256(clientIp(req) + req.client._peername.port + date) + date;
        if (req.cookies.token === generatedToken && tokens.findOne({token: generatedToken}) !== null) {
          next();
        } else {
          res.clearCookie("token");
          res.redirect("/edunet/signin?error=3");
          res.end();
        }
      }
    } else {
      if (req.cookies.token !== undefined && tokens.findOne({token: req.cookies.token}) !== null) {
        res.redirect("/edunet");
        res.end();
      } else {
        next();
      }
    }
  } else {
    next();
  }
}

module.exports = loginManager;
