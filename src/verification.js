//
// Edunet for WWW
// Verification schemas for Joi
//
// This code is under the MIT license.
//
const Joi = require("joi");
module.exports = {
  user: Joi.object().keys({
    username: Joi.string().alphanum().required(),
    password: Joi.string().required(),
    captcha: Joi.number().integer().required()
  }),
  post: Joi.object().keys({
    name: Joi.string().required(),
    postingUser: Joi.string().alphanum().required(),
    board: Joi.string().alphanum().required(),
    contents: Joi.string().min(5).required(),
    replies: Joi.array(),
    date: Joi.date().timestamp()
  }),
  incomingReply: Joi.object().keys({
    contents: Joi.string().required()
  });
};
