// Globals
const joi = require("joi");
const mongoose = require("mongoose");
const tokens = require("../database").tokens;
const marked = require("marked");
const winston = require("winston");

winston.loggers.add("reply-controller", {
  console: {
    level: "info",
    colorize: true,
    label: "reply-controller"
  }
});

const logger = winston.loggers.get("reply-controller");

module.exports = (app) => {
  app.post("/edunet/posts/*/*/*/reply", (req, res) => {
    const isValid = joi.validate(req.body, require("./verification").incomingReply);
    if(isValid.error !== null && isValid.error.isJoi === true) {
      // TODO Make the forms autocomplete
      res.redirect(req.url);
      res.end();
    } else {
      const user = req.url.split("/")[3];
      const board = req.url.split("/")[4];
      const post = req.url.split("/")[5];

      mongoose.model("posts", require("./database").schemas.post).findById(post)
        .then((newPost) => {
        newPost.replies.push({
          postingUser: tokens.findOne({token: req.cookies.token}).username,
          date: Date.now(),
          contents: marked(req.body.contents)
        });

        newPost.save();
        res.redirect("/edunet/posts/" + user + "/" + board + "/" + post);
        res.end();
      }).catch((err) => {
        logger.error(err);
        res.redirect("/edunet/posts/" + user + "/" + board + "/" + post);
        res.end();
      });
    }
  });
};
