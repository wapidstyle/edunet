//
// Edunet WWW
// Signing out controller
//
// This code is under the MIT license.
//
module.exports = (app) => {
  app.get("/edunet/signout$", (req, res) => {
    res.clearCookie("token");
    res.redirect("/edunet/signin");
    res.end();
  });
};
