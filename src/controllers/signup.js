// Globals
const getFile = require("../getfile");
const mongoose = require("mongoose");
const joi = require("joi");
const bcrypt = require("bcryptjs");
const winston = require("winston");
winston.loggers.add("signup-controller", {
  console: {
    level: "info",
    colorize: true,
    label: "signup-controller"
  }
});

const logger = winston.loggers.get("signup-controller");

module.exports = (app) => {
  app.get("/edunet/signup$", (req, res) => {
    getFile(res, "static/signup.html");
  });

  app.post("/edunet/signup$", (req, res) => {
    // Make Joi not ALWAYS cause an error
    req.body.captcha = 0;
    const isValid = joi.validate(req.body, require("./verification").user);
    if(isValid.error !== null) {
      res.redirect(req.url);
      res.end();
    } else {
      mongoose.model("users", require("./database").schemas.user).findOne({
        name: req.body.username
      }).then((data) => {
        if (data !== null) {
          res.redirect("/edunet/signup");
          res.end();
        } else {
          const User = mongoose.model("users", require("./database").schemas.user);
          let newUser = new User({
            name: req.body.username,
            pwd: bcrypt.hashSync(req.body.password, 8)
          });
          newUser.save();
          res.redirect("/edunet/signin");
          res.end();
        }
      }).catch((err) => {
        logger.error(err);
        res.redirect("/");
        res.end();
      });
    }
  });
};
