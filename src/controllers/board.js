const mongoose = require("mongoose");
const getFile = require("../getfile");
const _ = {
  isEmpty: require("lodash.isempty"),
  forEach: require("lodash.foreach")
};

module.exports = (app) => {
  app.get("/edunet/board/*[^/]", (req, res) => {
    const board = req.url.split("/")[3];
    mongoose.model("posts", require("./database").schemas.post).find({
      board: board
    }).exec().then((data) => {
      mongoose.model("boards", require("./database").schemas.board).find().exec().then((boardMeta) => {
        let html = "";
        if (_.isEmpty(data)) {
          html = "</ul><em>No posts yet in this group</em><ul>";
        } else {
          _.forEach(data, (element) => {
            html += "<li><a href=\"/edunet/posts/" + element.postingUser + "/" + board + "/" + element._id + "\"" +
                      ">" + element.name + "</a> by " + element.postingUser + " " +
                      "with " + element.replies.length + " replies";
          });
        }
        getFile(res, "static/main/board.html", {
          board: board,
          desc: boardMeta.message,
          posts: html
        });
      });
    });
  });
};
