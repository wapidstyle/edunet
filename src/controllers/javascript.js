//
// Edunet WWW
// JavaScript/CSS retreival controller
//
// This code is under the MIT license.
//
const winston = require("winston");
const getFile = require("../getfile");
winston.loggers.add("javascript-controller", {
  console: {
    level: "info",
    colorize: true,
    label: "js-controller"
  }
});

const logger = winston.loggers.get("javascript-controller");

module.exports = (app) => {
  app.use("/edunet/bower_components", (req, res) => {
    const url = req.url.replace("\..\g", ".");
    logger.info("Request to /edunet/bower_components" + req.url + ", masked ..." + url);
    getFile(res, "bower_components/" + url);
  });

  app.use("/edunet/js", (req, res) => {
    const url = req.url.replace("\..\g'", ".");
    logger.info("Request to /edunet/js" + req.url + ", masked ..." + url);
    getFile(res, "js/" + url);
  });
};
