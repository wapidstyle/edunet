// Globals
const mongoose = require("mongoose");
const getFile = require("../getfile");
const tokens = require("../database").tokens;
const _ = {
  forEach: require("lodash.foreach")
};

module.exports = (app) => {
  app.get("/edunet$", (req, res) => {
    mongoose.model("boards", require("./database").schemas.board).find().exec().then((data) => {
      let boards = [];
      _.forEach(data, (value) => {
        boards.push({
          name: value.name,
          message: typeof value.message !== "undefined" ? "&nbsp;" + value.message : ""
        });
      });
      boards = boards.sort();
      let html = "";

      _.forEach(boards, (value) => {
        html += "<li><a href=\"/edunet/board/" + value.name + "\">" + value.name + "</a>" + value.message;
      });
      getFile(res, "static/main/index.html", {
        username: tokens.findOne(req.cookies.token).username,
        boards: html
      });
    });
  });
};
