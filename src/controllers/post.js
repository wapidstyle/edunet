//
// Edunet WWW
// Post submission controllers
//
// This code is under the MIT license.
//

// Globals
const mongoose = require("mongoose");
const getFile = require("../getfile");
const joi = require("joi");
const tokens = require("../database").tokens;
const marked = require("marked");

module.exports = (app) => {
  app.get("/edunet/board/*/post", (req, res) => {
    const board = req.url.split("/")[3];
    mongoose.model("boards", require("./database").schemas.board).findOne({
      name: board
    }).then(data => {
      if (data === null) {
        res.redirect("/edunet");
        res.end();
      } else {
        getFile(res, "static/main/post-to-board.html", {
          board: board
        });
      }
    });
  });

  app.post("/edunet/board/*/post", (req, res) => {
    const isValid = joi.validate(req.body, require("./verification").post);
    if(isValid.error !== null && isValid.error.isJoi === true) {
      res.redirect(req.url);
      res.end();
    } else {
      const board = req.url.split("/")[3];
      mongoose.model("boards", require("./database").schemas.board).findOne({
        name: board
      }).then(data => {
        if (data === null) {
          res.redirect("/edunet");
          res.end();
        } else {
          const Post = mongoose.model("posts", require("./database").schemas.post);
          let newPost = new Post({
            name: req.body.title,
            postingUser: tokens.findOne({token: req.cookies.token}).username,
            board: board,
            contents: marked(req.body.contents),
            replies: [],
            date: Date.now()
          });
          newPost.save();
          res.redirect("/edunet/board/" + board);
          res.end();
        }
      });
    }
  });
};
