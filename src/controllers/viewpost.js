const mongoose = require("mongoose");
const moment = require("moment");
const getFile = require("../getfile");
const _ = {
  forEach: require("lodash.foreach")
};

module.exports = (app) => {
  app.get("/edunet/posts/*/*/*", (req, res) => {
    const user = req.url.split("/")[3];
    const board = req.url.split("/")[4];
    const post = req.url.split("/")[5];

    mongoose.model("posts", require("./database").schemas.post).findOne({
      postingUser: user,
      board: board,
      _id: post
    }).then((data) => {
      if (data !== null) {
        let replies = "";
        if(data.replies === []) {
          replies = "<em>No replies.</em>";
        } else {
          _.forEach(data.replies, (reply) => {
            replies += "<h5 style=\"margin-top:0;\">";
            replies += reply.postingUser;
            replies += " - <em>";
            replies += moment(reply.date).format("MMMM dddd Do YYYY, h:m A");
            replies += "</em></h5><p>";
            replies += reply.contents;
            replies += "</p>";
          });
        }
        getFile(res, "static/main/post.html", {
          user: user,
          board: board,
          contents: data.contents,
          title: data.name,
          date: moment(data.date).format("MMMM dddd Do YYYY, h:m A"),
          id: data._id,
          replies: replies
        });
      } else {
        getFile(res, "static/error/404.html");
      }
    });
  });
};
