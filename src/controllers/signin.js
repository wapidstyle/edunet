//
// Edunet WWW
// Signing in controllers
//
// This code is under the MIT license.
//

// Globals
const captchas = require("../database").captchas;
const _ = {
  random: require("lodash.random"),
  forEach: require("lodash.foreach")
};
const mex = require("math-expression-evaluator");
const clientIp = require("client-ip");
const getFile = require("../getfile");
const joi = require("joi");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const sha256 = require("js-sha256").sha256;
const tokens = require("../database").tokens;
const winston = require("winston");

winston.loggers.add("signin-controller", {
  console: {
    level: "info",
    colorize: true,
    label: "signin-controller"
  }
});

const logger = winston.loggers.get("signin-controller");

module.exports = (app) => {
  app.get("/edunet/signin$", (req, res) => {
    let error = "";
    switch (Number(req.query.error)) {
    case 0:
      error = "Invalid CAPTCHA!";
      break;
    case 1:
      error = "Missing field!";
      break;
    case 2:
      error = "User not found";
      break;
    case 3:
      error = "Invalid token";
      break;
    case 4:
      error = "An error occurred on our side while signing in; try again and";
      error += " if it still fails contact the administrator";
      break;
    case 5:
      error = "Invalid password";
      break;
    default:
      error = "Stop fooling around with GET parameters.";
      break;
  } // FIXME Why the heck is this happening?!?!?!?

    let question = String(_.random(0, 10, false));
    switch (Number(req.query.error)) {
      case 0:
        question += "+";
        break;
      case 1:
        question += "-";
        break;
      case 2:
        question += "*";
        break;
      case 3:
        question += "/";
        break;
      default:
        question += "+";
        break;
    }
    question += String(_.random(3, 7, false));
    captchas.insert({
      answer: mex.eval(question),
      id: clientIp(req) + ":" + req.client._peername.port
    });
    question.replace("/", "\u00f7");
    getFile(res, "static/signin.html", {
      error: error,
      displayError: req.query.error === undefined ? "none" : "block",
      equation: question
    });
  });

  app.post("/edunet/signin$", (req, res) => {
    let isValid = joi.validate(req.body, require("./verification").user);
    if (isValid.error !== null && isValid.error.isJoi === true) {
      let done = false;
      _.forEach(isValid.error.details, (element) => {
        if(!done) {
          switch(element.message) {
            case "\"username\" is not allowed":
              res.redirect("/edunet/signin?error=2");
              res.end();
              done = true;
              break;
            case "\"password\" is not allowed":
              res.redirect("/edunet/signin?error=5");
              res.end();
              done = true;
              break;
            case "\"captcha\" is not allowed":
              res.redirect("/edunet/signin?error=0");
              res.end();
              done = true;
              break;
          }
        }
      });
    } else if ((() => {
      try {
        return Number(req.body.captcha) === (Number(captchas.findOne({
          id: clientIp(req) + ":" + req.client._peername.port
        }).answer));
      } catch(e) {
        return false;
      }
    }).call()) {
      let User = mongoose.model("users", require("./database").schemas.user);
      User.findOne({
        name: req.body.username
      }, "name pwd").then((data) => {
        if (data === null) {
          res.redirect("/edunet/signin?error=2");
        } else {
          // TODO Use Argon2 instead of BCrypt
          // TODO Put this in a better way using more Promises
          if (bcrypt.compareSync(req.body.password, data.pwd)) {
            // Generate a token
            const token = String(sha256(clientIp(req) + req.client._peername.port + Date.now()) + Date.now());
            res.cookie("token", token);
            tokens.insert({
              token: token,
              username: data.name
            });
            res.redirect("/edunet");
            res.end();
          }
        }
      }).catch((err) => {
        logger.error("An error occurred while user was signing in:");
        logger.error(err);
        res.redirect("/edunet/signin?error=4");
      });
    } else {
      captchas.findAndRemove({
        id: clientIp(req) + ":" + req.client._peername.port
      });
      res.redirect("/edunet/signin?error=0");
    }
  });
};
