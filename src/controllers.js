//
// Edunet for WWW
// Main server (controllers only, see index.js for real server)
//
// This code is under the MIT license.
//
//jshint -W097
"use strict";
const winston = require("winston");
const q = require("q");
const express = require("express");
const clientIp = require("client-ip");
const mongoose = require("mongoose");
const getFile = require("./getfile");
const cache = require("flat-cache");
const LokiDatabase = require("lokijs");
const bcrypt = require("bcryptjs");
const sha256 = require("js-sha256").sha256;
const marked = require("marked");
const moment = require("moment");
const joi = require("joi");

marked.setOptions({
  highlight: (code, lang) => {
    return require("highlight.js").highlight(lang, code, true).value;
  },
  gfm: true,
  tables: true,
  breaks: true,
  smartypants: true
});

let db = new LokiDatabase("edunet-web.db");
let captchas = db.addCollection("captchas");
let tokens = db.addCollection("tokens");

require("./database").mainDb = db;
require("./database").captchas = captchas;
require("./database").tokens = tokens;

const _ = {
  random: require("lodash.random"),
  forEach: require("lodash.foreach"),
  isEmpty: require("lodash.isempty")
};

winston.loggers.add("controller", {
  console: {
    level: "info",
    colorize: true,
    label: "controller"
  }
});
winston.loggers.add("loginform", {
  console: {
    level: "info",
    colorize: true,
    label: "login-form"
  }
});

const logger = winston.loggers.get("controller");

function controllers(app, embedded) {
  // Allow cookie reading!
  app.use(require("cookie-parser")());
  // GIVE ME DEM FORMS!
  app.use(require("body-parser").urlencoded({
    extended: true
  }));

  // Check if URL starts with .html or index.html is requested
  app.use((req, res, next) => {
    if(req.url.endsWith("index.html")) {
      res.redirect(req.url.replace(/\/index.html/, ""));
      res.end();
    } else if(req.url.endsWith(".html")) {
      res.redirect(req.url.split(".html")[0]);
      res.end();
    } else {
      next();
    }
  });

  // Login management
  app.use(require("./login"));

  // Logger
  app.use((req, res, next) => {
    logger.info("Connection to " + req.url + " from " + clientIp(req) + " via method " + req.method);
    next();
  });

  // Main page controller
  require("./controllers/main")(app);

  // Sign in controller
  require("./controllers/signin")(app);

  // Post controller
  require("./controllers/post")(app);

  // Board controller
  require("./controllers/board")(app);

  // Post viewer controller
  require("./controllers/viewpost")(app);

  // Replying controller
  require("./controllers/reply")(app);

  // Signup controller
  require("./controllers/signup")(app);

  // JavaScript/Bower fetcher
  require("./controllers/javascript")(app);
}

module.exports = controllers;
