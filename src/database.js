//
// Edunet 1.0.0 database thingy
// Simplifies getting stuff from MongoDB, and stores data.
//
// This code is under the MIT License.
//
const mongoose = require("mongoose");

module.exports = {
  schemas: {
    user: mongoose.Schema({
      name: String,
      pwd: String // Hashed/bcrypted
    }),
    post: mongoose.Schema({
      name: String,
      postingUser: String,
      board: String,
      contents: String,
      replies: Array,
      date: Number
    }),
    board: mongoose.Schema({
      name: String,
      message: String
    })
  },
  // TODO find a better place to put this
  mainDb: null,
  captchas: null,
  tokens: null
};
