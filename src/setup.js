const chalk = require("chalk");
const fs = require("graceful-fs");
const mongoose = require("mongoose");
let pass = "";

console.log(chalk.green("Welcome to Edunet-web setup."));
console.log("This, right now, just puts your private key for Edunet into the MongoDB.");

require("readline").createInterface({
  input: process.stdin,
  output: process.stdout
}).question("Enter key passphrase: [WARNING: will echo!] ", (data) => {
  pass = data;
  mongoose.connect("mongodb://edunet:test@localhost:27017/edunet");
  const connection = mongoose.connection;
  console.log("Connecting to DB.....");

  var pgpKey = mongoose.Schema({
    name: String,
    contents: Buffer,
    passphrase: String
  });

  try {
    connection.on("open", () => {
      console.log("Connection");
      var PgpKey = mongoose.model("pgp_keys", pgpKey);
      new PgpKey({
        name: "private",
        contents: require("fs").readFileSync("private.key"),
        passphrase: pass
      }).save();
      console.log("Done uploading key");
    });
  } catch (e) {
    console.log("Please use the following command to export your key:");
    console.log(chalk.bold("gpg --export-secret-keys > private.key"));
    console.log(chalk.bold.red.bgYellow("MAKE SURE NOT TO TYPE ANY PASSWORDS FOR NON-EDUNET KEYS!"));
    console.log(chalk.bold.red("Just cancel it."));
  }
});
